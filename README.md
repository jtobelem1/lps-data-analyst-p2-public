# LPS - Data Analyst - P2

Ce repo est à destination des apprenant.e.s.

## Planning de la formation

![planning](planning.png)


## Contenu de la formation

- [X] [module 1](https://gitlab.com/jtobelem1/lps-data-analyst-p2-public/-/tree/master/module1) : Analyse des données (9j) + agilité (1j)
- [X] [module 2](https://gitlab.com/jtobelem1/lps-data-analyst-p2-public/-/tree/master/module2) : Data acquisition et intégration (5j)
- [X] [module 3](https://gitlab.com/jtobelem1/lps-data-analyst-p2-public/-/tree/master/module3) : Big data (3j)
- [X] [module 4](https://gitlab.com/jtobelem1/lps-data-analyst-p2-public/-/tree/master/module4) : Machine learning (5j)
- [X] [module 5](https://gitlab.com/jtobelem1/lps-data-analyst-p2-public/-/tree/master/module5) : Dataviz (5j)
- [ ] [projet pour la certification](https://gitlab.com/jtobelem1/lps-data-analyst-p2-public/-/tree/master/certification)

## Environnement technique de la formation
- local : python, tableau, dataspell
- distance : postgres, dataiku, jupyterhub, outils big data

## Liens gitlab pour la formation
- Les jeux de données : https://gitlab.com/jtobelem1/lps-data-analyst-p2-datasets
- Installation de l'environnement : https://gitlab.com/jtobelem1/lps-data-analyst-p2-env
- Les ressources de cours : https://gitlab.com/jtobelem1/lps-data-analyst-p2-public

## Liens serveur pour la formation
  - Dataiku DSS : http://simplon-formations-data.westeurope.cloudapp.azure.com
  - Jupyterhub : http://simplon-formations-data.westeurope.cloudapp.azure.com/jupyter
  - Postgres : pgserver-simplon.postgres.database.azure.com

## Liens Simplon pour la formation
- https://simplonline.co/
- https://app.sowesign.com/

# Certification

<https://www.francecompetences.fr/recherche/rs/3508/>

# Equipe Simplon

| Intervenant.e          | Rôle                     | Mail                             |
| ---------------------- | ------------------------ | -------------------------------- |
| Ana Manso              | Cheffe de projet         | <amanso@simplon.co>              |
| Jonathan Siffert       | Responsable opérationnel | <jsiffert@simplon.co>            |
| Dyonora Soares Semedo  | Support                  | <dsoares-semedo@simplon.co>      |
| Palmyre Roigt          | Formatrice               | <proigt@simplon.co>              |
| Manel Boumaiza         | Formatrice               | <mboumaiza.ext@simplon.co>       |
| David Azria            | Formateur                | <dazria.ext@simplon.co>          |
| Alexis Gladiline Bozio | Formateur                | <agladilinebozio.ext@simplon.co> |
| Josselin Tobelem       | Formateur référent       | <jtobelem@simplon.co>            |
