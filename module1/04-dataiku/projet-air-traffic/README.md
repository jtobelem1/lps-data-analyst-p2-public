# Airport Traffic by US and International Carriers

## Overview
### Business Case
As a research and analytics team at an international carrier, we need to create data pipelines to produce reports using publicly available U.S. Department of Transportation (USDOT) airport traffic data. Airline executives have been consuming this information indirectly through USDOT reports and additional assets produced by business analysts.

Our mandate is to create a data pipeline that drives a reporting system on international flights. The reporting system, including changes to the data pipeline, will be maintained by business analysts with help from the analytics team. Analysts often work on such pipelines that take larger datasets and shrink them into smaller dimensions. The goal of our team is to help them do so faster, more efficiently and in a reproducible manner.

### Supporting Data
The data comes from the U.S. International Air Passenger and Freight Statistics Report. As part of the T-100 program, USDOT receives traffic reports of US and international airlines operating to and from US airports. Data engineers on the team ingest this publicly available data and provide us with the following datasets:

- departures: Data on all flights between US gateways and non-US gateways, irrespective of origin and destination.
Each observation provides information on a specific airline for a pair of airports, one in the US and the other outside. Three main columns record the number of flights: Scheduled, Charter and Total.

- passengers: Data on the total number of passengers for each month and year between a pair of airports, as serviced by a particular airline.
The number is also broken down by those in scheduled flights plus those in chartered flights.

We will start with data for 2017.

## Workflow Overview
The final pipeline in Dataiku DSS is shown below. You can follow along with the final completed project in the Dataiku gallery.

The Flow has the following high-level steps:

Download the datasets to acquire the input data
Clean the passengers dataset, group by airport id, and find the busiest airports by number of passengers that year
Clean the departures dataset and turn the monthly information on airport pairs into market share data


## Detailed Walkthrough
Create a new blank Dataiku DSS project and name it International Flight Reporting.

### Finding the Busiest Airports by Volume of International Passengers
Let’s use a Download visual recipe to import the data.

1. In the Flow, select + Recipe > Visual > Download.
2. Name the output folder Passengers and create the recipe.
3. + Add a First Source and specify the following URL: https://data.transportation.gov/api/views/xgub-n9bw/rows.csv?accessType=DOWNLOAD.
4. Run the recipe to download the files.

Having downloaded the raw data, we now want to read it into DSS.

1. With the Passengers folder selected, choose Create a dataset from the Actions menu in the top right corner. This initiates a New Files in Folder dataset.
2. Click Test to let Dataiku detect the format and parse the data accordingly.
3. In the top right, change the dataset name to passengers and create.

Now let’s filter the data for our objectives.

1. With the new dataset as the input, create a new Sample/Filter recipe.
2. Turn filtering On and keep only rows where Year equals 2017.
3. Under the Sampling menu, choose No sampling (whole data).

After running the recipe to create the new dataset, let’s start to clean it. Start a Prepare visual recipe with the following steps in its script:

1. Parse the date_dte column into a proper date column.
- Dataiku should detect the correct date format as MM/dd/yyyy. If it does not, go ahead select it manually in the Smart date editor. Rename the output column date_parsed.
2. Identify the months using month names.
- One way to do so is with the Find and replace processor to replace the numerical values in the Month column with a new column called month_name. An example of a replacement is using “Jan” instead of the value “1”.