# Module 1 : analyse des données + introduction à l'agilité

*Ce module permet de présenter le cadre général d'un projet de data science, les outils principaux utilisés pendant la formation (BDD, python, dataiku). Ces outils sont utilisés pour un premier projet.*

## Intervenant.e.s
- Manel Boumaiza (3j)
- Josselin Tobelem (6j)
- Palmyre Roigt (1j)

## Contenu du module
- introduction à l'agilité
- introduction de la formation + rappels python/jupyter notebook
- 3 modules python de référence en datascience : numpy, pandas, seaborn
- bases de données : postgres, mongoDB
- séries temporelles
- dataiku
- projet

