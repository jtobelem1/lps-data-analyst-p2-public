# Module 2 : acquisition et intégration des données

*Ce module permet de présenter comment acquérir des données depuis plusieurs sources possibles (csv, xml, API, etc ...) afin de traiter les données puis les intégrer dans une source de données strucutrée.*

## Intervenant.e.s
- Alexis Gladiline (2j)
- Josselin Tobelem (3j)

## Contenu du module
- formats csv, json
- préparation des données
- dataiku pour acquisition et intégration
- bases de données non relationnelles
- projet
- (ouverture : données geospatiales)
- (ouverture : données volumineuses)
