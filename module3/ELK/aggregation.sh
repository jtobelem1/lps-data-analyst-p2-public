# Création de l'index 
PUT ecommerce_data
{
  "mappings": {
    "properties": {
      "Country": {
        "type": "keyword"
      },
      "CustomerID": {
        "type": "long"
      },
      "Description": {
        "type": "text"
      },
      "InvoiceDate": {
        "type": "date",
        "format": "M/d/yyyy H:m"
      },
      "InvoiceNo": {
        "type": "keyword"
      },
      "Quantity": {
        "type": "long"
      },
      "StockCode": {
        "type": "keyword"
      },
      "UnitPrice": {
        "type": "double"
      }
    }
  }
}

# Réindexaction de la donnée chargée
POST _reindex
{
  "source": {
    "index": "commerce_data"
  },
  "dest": {
    "index": "ecommerce_data"
  }
}

#Enlever les valeurs négatives du champs "UnitPrice"
POST ecommerce_data/_delete_by_query
{
  "query": {
    "range": {
      "UnitPrice": {
        "lte": 0
      }
    }
  }
}

#Enlever les valeurs au dessus de 500 du champs "UnitPrice"
POST ecommerce_data/_delete_by_query
{
  "query": {
    "range": {
      "UnitPrice": {
        "gte": 500
      }
    }
  }
}


# Aggrégation faire la somme de tous les prix des documents de l'index
GET ecommerce_data/_search
{
  "aggs": {
    "somme_prix": {
      "sum": {
        "field": "UnitPrice"
      }
    }
  }
}

# Si on n'est pas intéressé par les hit : on dit que la taille. On n'est pas intéressé par les 10 meilleurs résultats.
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "somme_prix": {
      "sum": {
        "field": "UnitPrice"
      }
    }
  }
}

# Minimum
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "plus_bas_prix": {
      "min": {
        "field": "UnitPrice"
      }
    }
  }
}

# Max
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "plus_haut_prix": {
      "max": {
        "field": "UnitPrice"
      }
    }
  }
}

#Moyenne
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "prix_moyen": {
      "avg": {
        "field": "UnitPrice"
      }
    }
  }
}

#Stats
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "stats_prix": {
      "stats": {
        "field": "UnitPrice"
      }
    }
  }
}

# Count Distinct
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "nombre_client_distinct": {
      "cardinality": {
        "field": "CustomerID"
      }
    }
  }
}

# Avoir uniquement la moyenne des prix en allemagne
GET ecommerce_data/_search
{
  "size": 0,
  "query": {
    "match": {
      "Country": "Germany"
    }
  },
  "aggs": {
    "prix_moyen_allemand": {
      "avg": {
        "field": "UnitPrice"
      }
    }
  }
}

# Pour calculer des aggrégation sur différents types de champs. Pour cela on fait des aggrégation par bucket.
# par exemple on a besoin d'avoir le revenu par mois.
# Tous les documents dans un même bucket partagent une même propriété.
# Plusieurs manières de regrouper les documents dans des bucker
# 1. Histogram Date aggregation
# Lorsque vous cherchez à regrouper des données par intervalle de temps, l'agrégation date_histogramme s'avère très utile !
# Notre index ecommerce_data contient des données de transaction qui ont été collectées au fil du temps (de l'année 2010 à 2011).
# Si nous cherchons à obtenir des informations sur les transactions dans le temps, notre premier réflexe devrait être d'exécuter l'agrégation date_histogram.
# Il existe deux façons de définir un intervalle de temps avec l'agrégation date_histogram :
# - fixed_interval
# - calendar_interval.
# Fixed_interval Avec le fixed_interval, l'intervalle est toujours constant.
# Exemple : Créer un bucket pour chaque intervalle de 8 heures.

GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "transactions_8_h": {
      "date_histogram": {
        "field": "InvoiceDate",
        "fixed_interval": "8h"
      }
    }
  }
}

# LA fenêtre de temps d'un mois.
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "transactions_par_mois": {
      "date_histogram": {
        "field": "InvoiceDate",
        "calendar_interval": "1M"
      }
    }
  }
}

# Changer l'ordre des documents.
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "transactions_par_mois": {
      "date_histogram": {
        "field": "InvoiceDate",
        "calendar_interval": "1M",
        "order": {
          "_key": "desc"
        }
      }
    }
  }
}

# Avoir des intervalles de prix de taille 10. Le nombre de documents diminuent avec l'augmentation du prix.
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "interval_prix": {
      "histogram": {
        "field": "UnitPrice",
        "interval": 10
      }
    }
  }
}

# Avoir les documents du plus cher au moins cher.
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "interval_prix": {
      "histogram": {
        "field": "UnitPrice",
        "interval": 10,
        "order": {
          "_key": "desc"
        }
      }
    }
  }
}


# Range aggregation : pouvoir faire varier la taille des intervalles choisis.
GET Enter_name_of_the_index_here/_search
{
  "size": 0,
  "aggs": {
   "Nom aggregation": {
      "range": {
        "field": "nom du champs aggrege",
        "ranges": [
          {
            "to": x
          },
          {
            "from": x,
            "to": y
          },
          {
            "from": z
          }
        ]
      }
    }
  }
}

# Intervalles de prix : de 0 à 50, de 50 à 200 ou au dela de 200.
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "interval_prix_var": {
      "range": {
        "field": "UnitPrice",
        "ranges": [
          {
            "to": 50
          },
          {
            "from": 50,
            "to": 200
          },
          {
            "from": 200
          }
        ]
      }
    }
  }
}

# Terms aggregation : avoir les client qui sont les plus importants
GET Enter_name_of_the_index_here/_search
{
  "aggs": {
    "Nom aggregation": {
      "range": {
        "field": "nom du champs aggrege",
        "size": State how many top results you want returned here
      }
    }
  }
}
Example:


# Avoir le top 5 des clients
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "top_5_clients": {
      "terms": {
        "field": "CustomerID",
        "size": 5
      }
    }
  }
}

# Avoir le revenu par jour. Il faut d'abord regrouper par jour et ensuite multiplier la quantité et le prix unitaire.
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "transaction_journaliere": {
      "date_histogram": {
        "field": "InvoiceDate",
        "calendar_interval": "day"
      },
      "aggs": {
        "revenu_jour": {
          "sum": {
            "script": {
              "source": "doc['UnitPrice'].value * doc['Quantity'].value"
            }
          }
        }
      }
    }
  }
}

# Avoir le revenu par jour. Il faut d'abord regrouper par jour et ensuite multiplier la quantité et le prix unitaire.
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "transaction_journaliere": {
      "date_histogram": {
        "field": "InvoiceDate",
        "calendar_interval": "day"
      },
      "aggs": {
        "revenu_jour": {
          "sum": {
            "script": {
              "source": "doc['UnitPrice'].value * doc['Quantity'].value"
            }
          }
        },
        "nombre_unique_client": {
          "cardinality": {
            "field": "CustomerID"
          }
        }
      }
    }
  }
}


# JE veux avoir le jour avec le revenu max.
GET ecommerce_data/_search
{
  "size": 0,
  "aggs": {
    "transaction_journaliere": {
      "date_histogram": {
        "field": "InvoiceDate",
        "calendar_interval": "day",
        "order": {
          "daily_revenue": "desc"
        }
      },
      "aggs": {
        "revenu_jour": {
          "sum": {
            "script": {
              "source": "doc['UnitPrice'].value * doc['Quantity'].value"
            }
          }
        },
        "nombre_unique_client": {
          "cardinality": {
            "field": "CustomerID"
          }
        }
      }
    }
  }
}
