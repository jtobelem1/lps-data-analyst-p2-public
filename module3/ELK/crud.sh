# Checker l'informations sur le cluster et les noeuds
GET _API/parameter

# Info sur la santé du cluster
GET _cluster/health

# Info sur les noeuds
GET _nodes/stats


# Création d'un index

PUT Nom-index

PUT bonbons

# Indexer un document
# Pour indexer un document, les deux verbes HTTP POST ou PUT peuvent être utilisés.

#Utilisez POST lorsque vous souhaitez qu'Elasticsearch génère automatiquement un identifiant pour votre document.

POST Nom-de-lindex/_doc
{
  "champ" : "valeur"
}
Exemple :

POST bonbons/_doc
{
  "prenom" : "Lisa",
  "bonbon" : "mars"
}

#Utilisez PUT lorsque vous souhaitez attribuer un identifiant spécifique à votre document (par exemple, si votre document possède un identifiant naturel - numéro de commande, numéro de patient, etc.) 

PUT Nom-index/_doc/id
{
  "champ" : "valeur"
}
Exemple :

PUT bonbons/_doc/1
{
  "prenom" : "Jean",
  "bonbon" : "twix"
}

#Lorsque vous indexez un document en utilisant un identifiant qui existe déjà, le document existant est écrasé par le nouveau document. Si vous ne voulez pas qu'un document existant soit écrasé, vous pouvez utiliser le point de terminaison _create !

#Avec le point de terminaison _create, aucune indexation n'aura lieu et vous obtiendrez un message d'erreur 409.

PUT Nom-index/_create/id
{
  "champ" : "valeur"
}

PUT bonbons/_create/1
{
  "prenom" : "Marc",
  "bonbon" : "haribo"
}

# R - LIRE
Lire un document

GET Nom-index/_doc/id-du-document


GET bonbons/_doc/1

# U - MISE À JOUR
# Mise à jour d'un document
# Si vous voulez mettre à jour les champs d'un document, utilisez la syntaxe suivante :

POST Nom-du-Index/_update/id-du-document-que-vous-voulez-mettre-à-jour
{
  "doc" : {
    "champs1" : "valeur",
    "champs2" : "valeur",
  }
} 

POST bonbons/_update/1
{
  "doc" : {
    "bonbons" : "M&M's"
  }
}

# D- SUPPRIMER

DELETE Nom-index/_doc/id-du-document-que-vous-voulez-supprimer

DELETE bonbons/_doc/1



