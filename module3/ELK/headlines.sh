# avoir les informations sur un index
GET news_data/_search

# Quel est le nombre de documents dans l'index ? Probablement pas 10000... Il s'agit d'une manière de garantir la rapidité du retour de la requête search.
# gte : greater or equal eq : equal. Pour avoir le nombre exact de documents

GET news_data/_search
{
  "track_total_hits": true
}


GET enter_name_of_the_index_here/_search
{
  "query": {
    "Specify the type of query here": {
      "Enter name of the field here": {
        "gte": "Enter lowest value of the range here",
        "lte": "Enter highest value of the range here"
      }
    }
  }
}

# Avoir toutes les news entre le 20/06 et le 22/09. 
# Les requêtes "query" servent à retrouver l'ensemble des documents qui correspondent aux critères de la requête.
# Il y a environ 8000 documents qui correspondent à notre recherche, plusieurs catégories différentes d'informations, on voudrait avoir des statistiques par catégorie.
GET news_data/_search
{
  "query": {
    "range": {
      "date": {
        "gte": "2015-06-20",
        "lte": "2015-09-22"
      }
    }
  }
}

# Pour avoir les types de catégories qui existent dans notre dataset, nous allons utiliser des aggrégations. 
GET nom_index/_search
{
  "aggregations": {
    "Nom de votre aggregation": {
      "specifier l'aggregation": {
        "field": "nom du champs que vous voulez aggréger",
        "size": combien de valeurs différentes de champs aggrégés 
      }
    }
  }
}

# Mon aggrégation s'appelle "by_catregory", je veux que cette aggrégation soit faite sur 100 occurences du champs. 
# Le type d'aggrégation pour avoir le nombre de document par index est "terms"
GET news_data/_search
{
  "aggs": {
    "par_categorie": {
      "terms": {
        "field": "category",
        "size": 100
      }
    }
  }
}

# Pour avoir les documents les plus populaires de la catégorie ENTERTAINMENT
# L'aggrégation va s'appeler populaire. Le type d'aggrégation est "significant_text". Le résultat de l'aggrégation sera dans le champs bucket.

GET news_data/_search
{
  "query": {
    "match": {
      "category": "ENTERTAINMENT"
    }
  },
  "aggregations": {
    "populaire": {
      "significant_text": {
        "field": "headline"
      }
    }
  }
}


# requete tousles documents qui contiennent ces mots dans le champs headline. Notre requête contient 4 termes, par défaut une requête relie les champs via un OR.
GET news_headlines/_search
{
  "query": {
    "match": {
      "headline": {
        "query": "Trump donald hillary clinton"
      }
    }
  }
}

# Augmenter la précision de notre requête.
GET news_headlines/_search
{
  "query": {
    "match": {
      "headline": {
        "query": "Trump donald hillary clinton",
        "operator": "and"
      }
    }
  }
}

# Pour ne pas être trop restrictif.
GET news_headlines/_search
{
  "query": {
    "match": {
      "headline": {
        "query": "Trump donald hillary clinton",
        "minimum_should_match": 3
      }
    }
  }
}


