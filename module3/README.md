# Module 3 : big data

*Le but de ce module est de donner une vision des enjeux du big data pour un data analyst ainsi que manipuler les mécanismes essentiels de stockage et calculs sur des architectures distribuées.*

## Intervenant.e.s
- Alexis Gladiline (3j)

## Contenu du module
- introduction au big data
- spark
- ELK
- projet
