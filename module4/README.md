# Module 4 : machine learning

*Le module “Machine Learning” vise à initier les apprenant.e.s aux outils et approches du Machine Learning : constitution d’un jeu de données adapté, conception d’un algorithme, optimisation de la performance et évaluation des métriques.*

## Intervenant.e.s
- David Azria (2j)
- Manel Boumaiza (0.5j)
- Josselin Tobelem (2.5j)

## Contenu du module
- introduction au machine learning
- principaux modèles
- métriques, généralisation
- dataiku pour le ML
- séries temporelles
- projet
