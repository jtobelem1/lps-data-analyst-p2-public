# Module 5 : dataz avancées

*Le module “Dataviz avancées” vise à aller plus loin dans la maîtrise des différents types de visualisations, en intégrant des graphiques plus spécifiques à la palette à disposition des apprenant.e.s.*

## Intervenant.e.s
- David Azria (2j)
- Josselin Tobelem (3j)

## Contenu du module
- seaborn
- widgets
- dash (ou autre librairie de dashboard python)
- tableau
- projet
